<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Mbclass extends Authenticatable
{
    protected $table="mbclass";
    public $timestamps=false;

}
