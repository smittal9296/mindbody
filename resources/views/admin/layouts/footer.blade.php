<footer class="footer text-right">
    @php echo date('Y'); @endphp © {{ config('app.name', 'Laravel') }}
</footer>