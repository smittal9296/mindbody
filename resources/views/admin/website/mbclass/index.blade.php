@extends('admin.layouts.base')
@section('breadcrumb')
<div id="top_bar" >
 
 <ul id="breadcrumbs" >
     <li><a href="{{ url('admin') }}" style=" color: rgba(0, 0, 0, 0.5); font-weight:600;">Dashboard</a></li>
     <li><span style="color:#2dc15f;">Class List</span></li>
 </ul>
 
</div>

@endsection
@section('content')
<style type="text/css">
.uk-badge-danger{
 background-color: #e53935;
}
.uk-text-center{
    text-transform:capitalize;
}
</style>

                           <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
         
            <div class="uk-margin-bottom">
          
         
             
      <a   href="{{URL('admin/addmbclass')}}">   <button class="md-btn uk-margin-left md-btn-primary" style="background:#2dc15f;"><i class="fa fa-plus"></i> Class</button></a>
     
      <a   href="{{URL('admin/csvdata')}}">   <button class="md-btn uk-margin-left md-btn-primary" style="background:#2dc1ad;"><i class="fa fa-list"></i> Report</button></a>
      
         </div>
         <div class="uk-overflow-container uk-margin-bottom">
             <table class="uk-table uk-table-align-vertical uk-table-nowrap " id="dt_default">
                 <thead>
                     <tr>
                     <th><input type="checkbox" id="selectall"></th>
                          <th>S. No.</th>
                         <th  class="uk-text-centers">Provider Name</th>
                         <th class=" uk-text-centers">Provider Email</th>  
                         <th class=" uk-text-centers">Provider Website</th>     
                         
                         <th class=" uk-text-centers">Total Click</th>     

                         <th class=" uk-text-centers">Provider Phoneno</th>
                         <th class=" uk-text-centers">Provider UUID</th>
                         <th class=" uk-text-centers">Class name</th>
                        
                          <th class=" uk-text-center">Action</th>
                     </tr>
                 </thead>
                 
                 <tbody>
                     
                     @foreach($data as $key => $d)
                     <tr>
                     <td><input type="checkbox" data-id="{{$d->id}}" class="checkboxattendence"></td>
                    
                             <td>{{ $key+1 }}</td>
                              <td class="uk-text-centers">{{str_replace(array('[',']','"'),' ',$d->providername) ?? ''}}</td>
                              <td class="uk-text-centers">
                                {{str_replace(array('[',']','"'),' ',$d->provideremail) ?? ''}}
                             </td>
                               <td class="uk-text-centers">
                            {{str_replace(array('[',']','"'),' ',$d->providerwebsite) ?? ''}}
                             </td>
                              <td class="uk-text-centers">
                                 {{$d->totalclick ?? '0'}}
                             </td>
                             <td class="uk-text-centers">
                       {{str_replace(array('[',']','"'),' ',$d->providerphoneno) ?? ''}}
                             </td>
                             <td class="uk-text-centers">
                              {{str_replace(array('[',']','"'),' ',$d->provideruuid) ?? ''}}
                             </td>
                             <td class="uk-text-centers">
                             {{$d->classname ?? ' '}} 
                             </td>
                            

                             
                             <td class="uk-text-center deleteParent">
                              
                                <a class="" href="{{URL('admin/editMbclass/'.base64_encode($d->id))}}">
                                    <i class="material-icons">edit</i></a>  
                                <a href="javascript:0;" class=""  onClick="mydelete({{$d->id}},'Mbclass')">
                                 <i class="material-icons md-20" style="color:#da154b">&#xE872;</i></a>
                             
                             </td>
                     </tr>
                     @endforeach
                   
                 </tbody>
             </table>
             <div style="display:flex;">
             <input type="button" value="Multiple Delete" class="md-btn uk-margin-left md-btn-primary" id="multiple_delete" style="display:none;background:#ceb648;">
             <br>
             <input type="button" value="Reset links" class="md-btn uk-margin-left md-btn-primary" id="resetlinks" style="display:none;background:rgb(206 72 72);">
            </div>
            </div>
    
        
     </div>
 </div>

 <form action="{{URL('admin/multipledelete')}}" method="POST" style="display: none;">
            @csrf
            <input type="hidden" name="id" id="delete_id"> 
            <input type="submit" name="submit" id="formsubmit">
    </form>

    <form action="{{URL('admin/resetlinks')}}" method="POST" style="display: none;">
            @csrf
            <input type="hidden" name="id" id="delete_id1"> 
            <input type="submit" name="submit" id="formsubmit1">
    </form>
@endsection

@section('js')
<script>
      $(document).on('click','#selectall',function(e){
        $('#multiple_delete').css('display','block');
        $('#resetlinks').css('display','block');
            if(this.checked) {
            $('.checkboxattendence').each(function(){
            $(".checkboxattendence").prop('checked', true);
        })
            }else {
            $('.checkboxattendence').each(function(){
            $(".checkboxattendence").prop('checked', false);
        })
            
            }                
        });
        $('.checkboxattendence').click(function(){
            $('#multiple_delete').css('display','block');
            $('#resetlinks').css('display','block');
            if($(".checkboxattendence").length == $(".checkboxattendence:checked").length) { 
                //if the length is same then untick 
                $("#selectall").prop("checked", true);
            }else {
                //vise versa
                $("#selectall").prop("checked", false);            
            }
        }); 

        $('#multiple_delete').click(function(e){
                   var i = 0;
                           
                   if($(".checkboxattendence:checked").length){
                        var arr = [];
                        
                       $('.checkboxattendence:checked').each(function () {
                            arr[i++] = $(this).data('id');
                          }); 
                         
                        //   alert(arr2);
                          $('#delete_id').val(arr);
                       $('#formsubmit').trigger('click');  
                   }else{
                    alert('Select Atleast 1 record');
                  }
                   
  
          }); 


          $('#resetlinks').click(function(e){
                   var i = 0;
                           
                   if($(".checkboxattendence:checked").length){
                        var arr = [];
                        
                       $('.checkboxattendence:checked').each(function () {
                            arr[i++] = $(this).data('id');
                          }); 
                         
                        //   alert(arr2);
                          $('#delete_id1').val(arr);
                         $('#formsubmit1').trigger('click');  
                   }else{
                    alert('Select Atleast 1 record');
                  }
                   
  
          }); 
  
</script>
@endsection