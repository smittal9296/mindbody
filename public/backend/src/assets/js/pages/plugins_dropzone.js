/*
*  altair admin
*  @version v2.20.1
*  @author tzd
*  @license http://themeforest.net/licenses
*  plugins_dropzone.js - plugins_dropzone.html
*/

$(function() {
    $("div#js-dropzone").dropzone({
        url: "/",
        paramName: "file",
        maxFilesize: 2,
        addRemoveLinks: true,
        renameFile: function(file) {
            return 'file_'+(new Date()).getTime();
        }
    });
});