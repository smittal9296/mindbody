<?php

Route::group(['namespace' => 'Admin'], function() {

    Route::get('{page?}', 'DashboardController@index')->name('home')->where('page', 'dashboard');
 
    Route::get('addmbclass','MbclassController@create');
    Route::get('mbclass','MbclassController@index');
    Route::POST('mbclassAdd','MbclassController@save');
    
    Route::get('editMbclass/{id}','MbclassController@edit');
    Route::get('/viewproduct',function(){
      return view('admin.website.product.add');
    });

    Route::POST('product','MbclassController@product');
    Route::get('scraping','MbclassController@scraping');
    Route::POST('scrapingdata','MbclassController@scrapingdata');
    Route::POST('shopifyscraping','MbclassController@shopifyscraping');
    Route::get('scrapinglist','MbclassController@scrapinglist');
    
    Route::POST('multipledelete','MbclassController@multipledelete');

     Route::get('cronbitly','MbclassController@cronbitly');

     Route::get('/resetlink/{id}','MbclassController@resetlink');
       
    Route::POST('resetlinks','MbclassController@resetlinks'); 

    Route::get('/csvdata','MbclassController@csvdata');
    });

