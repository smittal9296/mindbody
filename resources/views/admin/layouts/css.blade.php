@php
    $bower_url = url('backend/bower_components');
    $kendo_url = '../backend/bower_components/kendo-ui/js';
    $dist_min = isset($_GET["generate"]) ? '.min' : '';
    $srcAssets = url('backend');
@endphp
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300" rel="stylesheet">
<!-- weather icons -->
<link rel="stylesheet" href="{{ $bower_url.'/weather-icons/css/weather-icons'.$dist_min.'.css' }}" media="all">
<!-- metrics graphics (charts) -->
<link rel="stylesheet" href="{{ $bower_url.'/metrics-graphics/dist/metricsgraphics.css' }}">
<!-- chartist -->
<link rel="stylesheet" href="{{ $bower_url.'/chartist/dist/chartist.min.css' }}">

<!-- htmleditor (codeMirror) -->
<link rel="stylesheet" href="{{ $bower_url.'/codemirror/lib/codemirror.css' }}">

<!-- jquery ui -->
<link rel="stylesheet" href="{{ $srcAssets.'/assets/skins/jquery-ui/material/jquery-ui'.$dist_min.'.css' }}">

<!-- select2 -->
<link rel="stylesheet" href="{{ $bower_url.'/select2/dist/css/select2.min.css' }}">

<!-- uikit -->
<link rel="stylesheet" href="{{ $bower_url.'/uikit/css/uikit.almost-flat'.$dist_min.'.css' }}" media="all">

<!-- flag icons -->
<link rel="stylesheet" href="{{ $srcAssets.'/assets/icons/flags/flags'.$dist_min.'.css' }}" media="all">

<!-- style switcher -->
<link rel="stylesheet" href="{{ $srcAssets.'/assets/css/style_switcher.min.css' }}" media="all">


{{-- toaster --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha256-WolrNTZ9lY0QL5f0/Qi1yw3RGnDLig2HVLYkrshm7Y0=" crossorigin="anonymous" />

<link rel="stylesheet" href="{{ asset('backend/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/bower_components/cropper/dist/cropper.min.css') }}">

<!-- altair admin -->
<link rel="stylesheet" href="{{ $srcAssets.'/assets/css/main'.$dist_min.'.css' }}" media="all">

<!-- themes -->
<link rel="stylesheet" href="{{ $srcAssets.'/assets/css/themes/themes_combined.min.css' }}" media="all">

<link rel="stylesheet" href="{{URL('backend/summernote/summernote.css')}}" media="all">


<link rel="stylesheet" type="text/css" href="{{URL('backend/custom/sweetalert.css')}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<style>
    * {
  font-family: 'Montserrat', sans-serif;
}
    .md-input-wrapper > label.error{
        top:unset;
        color:firebrick;
    }

   .tablesorter-headerRow{ background:linear-gradient(180deg,#1B5E,#FFEE58);
   }
.tablesorter-headerRow > th {
	color:white !important;
}
	/*thead{
        background:#2dc15f;
    }*/
       .status{
       cursor: pointer;
    }
  /*  th{
color:white !important;
    }*/
    .uk-badge{
    	background: #2dc15f;
    }
    .uk-pagination > li.uk-active > a{
    background: #2dc15f;
    }
</style>