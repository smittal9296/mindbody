{{-- Admin Sidebar --}}

<style type="text/css">
    
#sidebar_main  .menu_section    ul > li > a {
    font: 500 14px / 25px , sans-serif;
    color: #ceb648;
}
#sidebar_main  .menu_section    ul > li > a > .menu_icon {
     width: 30px;
     font-size: 18px;
    color: #0f8140;
}

#sidebar_main  .menu_section    ul > li > ul> li > a {
    font: 500 14px / 25px "Roboto", sans-serif;
    color: #ceb648;
}
#sidebar_main .menu_section > ul > li.current_section > a .menu_title {
color:#2dc15f;
}
#sidebar_main .sidebar_main_header{
    background: none;
    position: relative;
    margin-bottom: 10px;
    /* height: 75px; */
    border-bottom: 1px solid #2dc15f
}
</style>

<!-- main sidebar -->
<aside id="sidebar_main">

    <div class="sidebar_main_header" >
        
        <img src="{{URL('image/logo.png')}}" style="height: 80px;
        margin-left: 25px;">
        
    </div>

    <div class="menu_section">
        <ul>
            <li  @isset($title) @if($title=='Dashboard') class="current_section" @endif @endisset title="Dashboard">
                <a href="{{URL('admin')}}">
                    <span class="menu_icon">
                        <i class="fa fa-home"></i></span>
                    <span class="menu_title">{{ __('Dashboard') }}</span>
                </a>
                
            </li>

            <li  @isset($title)@if($title=='Mbclass') class="current_section"@endif @endisset title="Mbclass">
                <a href="{{url('admin/mbclass')}}" >
                    <span class="menu_icon ">
                    <i class="fas fa-box"></i></span>
                    <span class="menu_title">Class</span>
                </a>
                
            </li>

            {{-- <li  @isset($title)@if($title=='viewproduct') class="current_section" @endif @endisset title="viewproduct">
                <a href="{{url('admin/viewproduct')}}" >
                    <span class="menu_icon ">
                    <i class="fas fa-box"></i></span>
                    <span class="menu_title">Product</span>
                </a>
                
            </li> --}}
            <li  @isset($title)@if($title=='Scraping') class="current_section" @endif @endisset title="Scraping">
                <a href="{{url('admin/scraping')}}" >
                    <span class="menu_icon ">
                    <i class="fas fa-box"></i></span>
                    <span class="menu_title">Scraping</span>
                </a>
                
            </li>
            <!-- <li  @isset($title)@if($title=='Scrapinglist') class="current_section" @endif @endisset title="Scrapinglist">
                <a href="{{url('admin/scrapinglist')}}" >
                    <span class="menu_icon ">
                    <i class="fas fa-box"></i></span>
                    <span class="menu_title">Scraping Data</span>
                </a>
                
            </li> -->
            
          
           
 

             


             
           
        </ul>
    </div>
</aside><!-- main sidebar end -->