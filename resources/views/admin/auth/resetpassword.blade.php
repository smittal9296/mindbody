<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> 
<html lang="en"> <!--<![endif]-->
<head>
    @include('admin.layouts.meta')    

    <title>{{ config('app.name', 'Laravel') }}</title>
    {{-- <meta http-equiv="refresh" content="100"> --}}
    <script>pageData={};window.pageData.baseUrl="{{ url('/') }}";</script>

    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ asset('backend/bower_components/uikit/css/uikit.almost-flat.min.css') }}"/>
{{-- toaster --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha256-WolrNTZ9lY0QL5f0/Qi1yw3RGnDLig2HVLYkrshm7Y0=" crossorigin="anonymous" />

    <link rel="stylesheet" href="{{ asset('backend/assets/css/login_page.min.css') }}" />
</head>
<body class="login_page" style="background-image: url({{url('image/gbackground.jpg')}});background-size: cover;">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card" style="background:#ffffff9c;">
            <div class="md-card-content large-padding" id="login_form" >
                <div class="login_heading">
                  <img src="{{URL('image/logo.png')}}" style="width: 150px;">
                    <!-- <span style="color:#2ea91d;font-size: 18px;">Welcome to {{ config('app.name', 'Laravel') }}</span> -->
               
                </div>
                <form method="POST" action="{{route('resetpasswordemail')}}" >
                     @csrf
                    <input type="hidden" name="token" value="{{$token}}">
                   <div class="uk-form-row">
                        <label for="login_password">Password</label>
                        <input class="md-input{{ $errors->has('password') ? ' is-invalid' : '' }}"  min="6" id="passwordcheck" type="password" name="password" required />

                     
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Confirm Password</label>
                        <input class="md-input{{ $errors->has('password') ? ' is-invalid' : '' }}" min="6" id="confirmpassword" type="password" name="confirmpassword" required />
                        <span class="error" style="color:red;"></span>
                    </div>
                    <div class="uk-margin-medium-top">
                        <input type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large" id="update" value=" Reset"  style="background-color: #5b8c6a">
                           
                    </div>
                </form>
            </div>
            
        
   

 
            </div>
             </div>

    <!-- common functions -->
    <script src="{{ asset('backend/assets/js/common.min.js') }}"></script>
    <!-- uikit functions -->
    <script src="{{ asset('backend/assets/js/uikit_custom.min.js') }}"></script>
    <!-- altair core functions -->
    <script src="{{ asset('backend/assets/js/altair_admin_common.min.js') }}"></script>

    <!-- altair login page functions -->
    <script src="{{ asset('backend/assets/js/pages/login.min.js') }}"></script>

    {{-- toast --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha256-56zsTlMwzGRtLC4t51alLh5cKYvi0hnbhEXQTVU/zZQ=" crossorigin="anonymous"></script>

<script>
        
    $('#confirmpassword').focusout(function(e){
       $cp=$(this).val();
       if($cp){
           $pw=$('#passwordcheck').val();
           if($pw){
               if($pw===$cp)
               {
                   console.log('check');
               $('#update').removeAttr('disabled');
               }else{
                   $('#update').attr('disabled',true);
                   $('.error').text('Password Does not Match');         
               }

           }else{
               $('#update').attr('disabled',true);
               $('.error').text('please Fill first Password');
           }

       }
    });
       </script>

@if (session('success'))
<script type="text/javascript">
    $.toast({
        heading: 'Success',
        text: "{{ session('success') }}",
        icon: 'success',
        loader: true,
        loaderBg: '#9EC600',
        allowToastClose: true,
        position : 'bottom-left'
    });
</script>
@php
    session()->forget('success');
@endphp
@endif

@if (session('error'))
<script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "{{ session('error') }}",
        icon: 'success',
        loader: true,
        loaderBg: '#9EC600',
        allowToastClose: true,
        position : 'bottom-left'
    });    
</script>
@php
    session()->forget('error');
@endphp
@endif
</body>
</html>

