
 @extends('admin.layouts.base')
 @section('breadcrumb')
<div id="top_bar" >
    
    <ul id="breadcrumbs" >
        <li><a href="{{ url('admin') }}" style=" color: rgba(0, 0, 0, 0.5); font-weight:600;">Dashboard</a></li>
        <li><span style="color:#2dc15f;">{{ isset($data) ? 'Update' : 'Add' }} Classes </span></li>
    </ul>
    
</div>
@endsection

@section('css')

<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
@endsection

@section('content')

<form id="form_validation" action="{{URL('admin/mbclassAdd')}}" method="POST" class="uk-form-stacked" enctype="multipart/form-data">
	@csrf

 <input type="hidden" name="id" value="{{$data->id ?? ''}}">
	<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
		<div class="uk-width-medium-1-1">
			<div class="md-card" >
				
				<div class="md-card-content large-padding">
				<div id="CoupansWrapper" class="CoupansWrapper uk-margin-bottom-large">
                            <input type="hidden" name="sectorJsonData" id="coupanJsonData">
				 <div class="CoupansBox">
					 @php $size=count(json_decode($data->providername)); @endphp

                     @for($i=0;$i<$size;$i++)
                     <hr/>
                   <div class="uk-grid uk-grid-divider CoupansContainer uk-grid-medium CurrencyCounvertorWrapper" data-uk-grid-margin>
				           
							<div class="uk-width-large-9-10">
								<div class="uk-grid uk-grid-divider uk-grid-medium uk-margin" data-uk-grid-margin>
														
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
											<label>Provider name</label>
												<input type="text" class="md-input" name="providername[]"  required value="{{json_decode($data->providername)[$i] }}">
										
											</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Website</label>
											<input type="text" class="md-input" name="providerwebsite[]" value="{{json_decode($data->providerwebsite)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Email</label>
											<input type="email" class="md-input " name="provideremail[]"   value="{{json_decode($data->provideremail)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Phone no</label>
											<input type="text" class="md-input onlynumber" name="providerphoneno[]"  value="{{json_decode($data->providerphoneno)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider UUID</label>
											<input type="text" class="md-input " name="provideruuid[]"   value="{{json_decode($data->provideruuid)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider  Description </label>
										<textarea class="md-input" name="providerdescription[]" >{{json_decode($data->providerdescription)[$i]}}</textarea>
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Type</label>
											<select name="providertype[]" class="md-input"  >
										
											<option name="spot" {{json_decode($data->providertype)[$i]=='spot' ? 'selected':''}}>Spot</option>
											<option name="stem" {{json_decode($data->providertype)[$i]=='stem' ? 'selected':''}}>Stem</option>
											<option name="gymnastics" {{json_decode($data->providertype)[$i]=='gymnastics' ? 'selected':''}}>Gymnastics</option>
											<option name="dance" {{json_decode($data->providertype)[$i]=='dance' ? 'selected':''}}>Dance</option>
											<option name="art" {{json_decode($data->providertype)[$i]=='art' ? 'selected':''}}>Art</option>
											<option name="theater" {{json_decode($data->providertype)[$i]=='theater' ? 'selected':''}}>Theater</option>
											<option name="other" {{json_decode($data->providertype)[$i]=='other' ? 'selected':''}}>Other</option>
										

											</select>
									
										</div>
									</div>
									<div class="uk-width-large-1-2">
										<div class="uk-form-row">
										<label>Provider Type option</label>
											<input type="text" class="md-input " name="providertypeoption[]"  value="{{json_decode($data->providertypeoption)[$i]}}">
									
										</div>
									</div>
									
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>Street Address</label>
											<input type="text" class="md-input " name="streetaddress[]"  value="{{json_decode($data->streetaddress)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>Apt/Room/Description </label>
											<input type="text" class="md-input " name="apt[]"  value="{{json_decode($data->apt)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>City</label>
											<input type="text" class="md-input " name="city[]"  value="{{json_decode($data->city)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>State</label>
											<input type="text" class="md-input " name="state[]"  value="{{json_decode($data->state)[$i]}}">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>Zip</label>
											<input type="text" class="md-input onlynumber" name="zip[]"  value="{{json_decode($data->zip)[$i]}}">
									
										</div>
									</div>
									</div>
								</div>
								<div class="uk-width-large-1-10">
										<div  class="md-btn md-btn-primary removeCoupan" data-uk-tooltip title="Remove Details " style="margin-left: -25px"><i class="material-icons"> close </i></div>
								</div>

									
									

								</div>
                                @endfor
							</div>

								<div class="uk-margin-large uk-margin-top">
									<div type="button" id="addCoupansButton" class="md-btn md-btn-primary" data-uk-tooltip title="Add More" >Add More </div>
								</div> 
							</div>
                            
                           
                    </div>
               </div>
</div>
  </div>
<!-- Start 2nd block -->
	<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
    	<div class="uk-width-medium-1-1">
	      
			<div class="md-card">		
					<div class="md-card-content large-padding">

					<div class="uk-grid uk-grid-divider uk-grid-medium CurrencyCounvertorWrapper" data-uk-grid-margin>
			
						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Class Name</label>
								<input type="text" class="md-input " name="classname"  value="{{@$data->classname ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Dates of the Class</label>
								<input type="date" class="md-input label-fixed" name="classdate"  value="{{@$data->classdate ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Age of Child</label>
								<Select name="classage" class="md-input" >
                                 <option name="0-12 months" {{@$data->classweek=='0-12 months'?'selected':''}} >0-12 months</option>     
								 <option name="12-24 months"  {{@$data->classweek=='12-24 months'?'selected':''}}>12-24 months</option>     
								 <option name="2-3 years"  {{@$data->classweek=='2-3 years'?'selected':''}}> 2-3 Years</option>     
								 <option name="3-4 years"  {{@$data->classweek=='3-4 years'?'selected':''}}>3-4 Years</option>     
								 <option name="4-5 years"  {{@$data->classweek=='4-5 years'?'selected':''}}>4-5 Years</option>     
								 <option name="5-6 years"  {{@$data->classweek=='5-6 years'?'selected':''}}>5-6 Years</option>     
								 <option name="6-7 years"  {{@$data->classweek=='6-7 years'?'selected':''}}>6-7 Years</option>     
								 <option name="7-8 years"  {{@$data->classweek=='7-8 years'?'selected':''}}>7-8 Years</option>     
								 <option name="8-9 years"  {{@$data->classweek=='8-9 years'?'selected':''}}>8-9 Years</option>     
								 <option name="9-10 years"  {{@$data->classweek=='9-10 years'?'selected':''}}>9-10 Years</option>     
								 <option name="10-11 years"  {{@$data->classweek=='10-11 years'?'selected':''}}>10-11 Years</option>     
								 <option name="12-13 years"  {{@$data->classweek=='12-13 years'?'selected':''}}>12-13 Years</option>     
								 <option name="13-14 years"  {{@$data->classweek=='13-14 years'?'selected':''}}>13-14 Years</option>     
								 <option name="14-15 years"  {{@$data->classweek=='14-15 years'?'selected':''}}>14-15 Years</option>     
								  <option name="15-18 years"  {{@$data->classweek=='15-18 years'?'selected':''}}>15-18 Years</option>     
							  
								</select>
						
							</div>
						</div>


						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Day of the Week</label>
								<Select name="classweek" class="md-input" >
                                 <option name="Weekday Only" {{@$data->classweek=='Weekday Only'?'selected':''}}>Weekday Only</option>     
								 <option name="Weekend Only" {{@$data->classweek=='Weekend Only'?'selected':''}}>Weekend Only</option>     
								 <option name="Monday" {{@$data->classweek=='Monday'?'selected':''}}>Monday</option>     
								 <option name="Tuesday" {{@$data->classweek=='Tuesday'?'selected':''}}>Tuesday</option>     
								 <option name="Wednesday" {{@$data->classweek=='Wednesday'?'selected':''}}>Wednesday</option>     
								 <option name="Thursday" {{@$data->classweek=='Thursday'?'selected':''}}>Thursday</option>     
								 <option name="Friday" {{@$data->classweek=='Friday'?'selected':''}}>Friday</option>     
								 <option name="Saturday" {{@$data->classweek=='Saturday'?'selected':''}}>Saturday</option>     
								 <option name="Sunday" {{@$data->classweek=='Sunday'?'selected':''}}>Sunday</option>     
							  
								</select>
						
							</div>
						</div>

						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Time of the Class</label>
								<Select name="classtime" class="md-input" >
                                 <option name="Morning 8am - 12pm" {{@$data->classtime=='Morning 8am - 12pm'?'selected':''}} >Morning 8am - 12pm</option>     
								 <option name="Early afternoon 12-3pm" {{@$data->classtime=='Early afternoon 12-3pm'?'selected':''}}>Early afternoon 12-3pm</option>     
								 <option name="After school 3-5pm" {{@$data->classtime=='After school 3-5pm'?'selected':''}}>After school 3-5pm</option>     
								 <option name="Evening 5-9pm" {{@$data->classtime=='Evening 5-9pm'?'selected':''}}>Evening 5-9pm</option>     
								 
								</select>
						
							</div>
						</div>

					


						<div class="uk-width-large-1-5">
							<div class="uk-form-row">
							<label>Street Address</label>
								<input type="text" class="md-input " name="classstreetaddress"  value="{{@$data->classstreetaddress ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-5">
							<div class="uk-form-row">
							<label>Address Details </label>
								<input type="text" class="md-input " name="classapt"  value="{{@$data->classapt ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-5">
							<div class="uk-form-row">
							<label>City</label>
								<input type="text" class="md-input " name="classcity"  value="{{@$data->classcity ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-5">
							<div class="uk-form-row">
							<label>State</label>
								<input type="text" class="md-input " name="classstate"  value="{{@$data->classstate ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-5">
							<div class="uk-form-row">
							<label>Zip</label>
								<input type="text" class="md-input onlynumber " name="classzip" value="{{@$data->classzip ?? ''}}">
						
							</div>
						</div>

						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Class Frequency</label>
								<Select name="classfrequency" class="md-input" > 
                                 <option name="Once" {{@$data->classfrequency=='Once'?'selected':''}}>Once</option>     
								 <option name="Session" {{@$data->classfrequency=='Session'?'selected':''}}>Session</option>     
								 <option name="Full week" {{@$data->classfrequency=='Full week'?'selected':''}}>Full week</option>     
								 <option name="Multi-week" {{@$data->classfrequency=='Multi-week'?'selected':''}}>Multi-week</option>     
								 
								</select>
						
							</div>
                       </div>

						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Season</label>
								<Select name="classseason" class="md-input" >
                                 <option name="Winter" {{@$data->classseason=='Winter'?'selected':''}}>Winter</option>     
								 <option name="Spring" {{@$data->classseason=='Spring'?'selected':''}}>Spring</option>     
								 <option name="Summer" {{@$data->classseason=='Summer'?'selected':''}}>Summer</option>     
								 <option name="Fall" {{@$data->classseason=='Fall'?'selected':''}}>Fall</option>     
								 
								</select>
						
							</div>
                         </div>

						 <div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Price</label>
								<Select name="classprice" class="md-input" >
                                 <option name="Monthly" {{@$data->classprice=='Monthly'?'selected':''}}>Monthly</option>     
								 <option name="Seasonal" {{@$data->classprice=='Seasonal'?'selected':''}}>Seasonal</option>     
								 <option name="Per Session" {{@$data->classprice=='Per Session'?'selected':''}}>Per Session</option>     
								 <option name="Per Class" {{@$data->classprice=='Per Class'?'selected':''}}>Per Class</option>     
								 
								</select>
						
							</div>
                         </div>

						 <div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Spaces Available</label>
								<input type="text" class="md-input " name="spacesavailable"  value="{{@$data->spacesavailable ?? ''}}">
						
							</div>
						</div>

					
						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Discount Code (Optional)</label>
								<input type="text" class="md-input " name="discountcode"  value="{{@$data->discountcode ?? ''}}">
						
							</div>
						</div>

						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Waiver link </label>
								<input type="text" class="md-input " name="waiverlink"  value="{{@$data->waiverlink ?? ''}}">
						
							</div>
						</div>
                        <div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Waitlist  </label>
								<input type="text" class="md-input " name="waitlist"  value="{{@$data->waitlist ?? ''}}">
						
							</div>
						</div>

						<div class="uk-width-large-1-3">
							<div class="uk-form-row">
							<label>Rules/Restriction  </label>
								<input type="text" class="md-input " name="rules"   value="{{@$data->rules ?? ''}}">
						
							</div>
						</div>
                        
						<div class="uk-width-large-1-2">
							<div class="uk-form-row">
							<label>Class Description  </label>
								<textarea name="classdescription" class="md-input summernote">{{@$data->classdescription ?? ''}}</textarea>
						
							</div>
						</div>

						<div class="uk-width-large-1-2">
							<div class="uk-form-row">
							<label>Cancellation Policy  </label>
								<textarea name="cancellationpolicy"  class="md-input summernote1">{{@$data->cancellationpolicy ?? ''}}</textarea>
						
							</div>
						</div>

						<div class="uk-width-large-1-2">
							<div class="uk-form-row">
							<label>Makeup  Policy  </label>
								<textarea name="makeuppolicy"  class="md-input summernote2">{{@$data->makeuppolicy ?? ''}}</textarea>
						
							</div>
						</div>
						<div class="uk-width-large-1-2">
							<div class="uk-form-row">
							<label>Soldout Option  </label>
								<input type="text" class="md-input " name="soldout"   value="{{@$data->soldout ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-2">
							<div class="uk-form-row">
							<label>With Developer Sports Available  </label>
								<input type="text" class="md-input " name="spotsavailable"   value="{{@$data->spotsavailable ?? ''}}">
						
							</div>
						</div>
						<div class="uk-width-large-1-2">
							<div class="uk-form-row">
							<label>Class Type</label>
								<Select name="classtype" class="md-input" >
                                 <option name="single/daily" {{@$data->classtype=='single/daily'?'selected':''}}>Single/daily</option>     
								 <option name="camps weekly" {{@$data->classtype=='camps weekly'?'selected':''}}>Camps weekly</option>     
								 <option name="reoccurring once a week" {{@$data->classtype=='reoccurring once a week'?'selected':''}}>Reoccurring once a week</option>     
								     
								 
								</select>
						
							</div>
                         </div>

						 <div class="uk-form-row uk-margin uk-padding">
							<button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" style="background:#2dc15f;">  Save  </button>
						</div>

						<div class="uk-form-row uk-margin uk-padding">
							<a class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" style="background:#2dc15f;">  Reset Clicks </a>
						</div>
					   
					  </div>
					</div>
					
				</div>
			</div>

		</div>
	</div>
</form>


@endsection


@section('js')

<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
<script type="text/javascript">


var sports=['Basketball','Football','Soccer','Hockey','Swimming','FieldHockey','Lacrosse','Baseball','Softball','Tennis','AnySpot'];
var Dance=['Ballet','Tap','Hiphop','Jazz','Contemporary','AnyDance'];


        $('.summernote').summernote({
        placeholder: 'Enter Class Description',
        tabsize: 2,
        height: 100
      });
	  $('.summernote1').summernote({
        placeholder: 'Enter Cancelation Policy',
        tabsize: 2,
        height: 100
      });
	  $('.summernote2').summernote({
        placeholder: 'Enter Makeup Policy ',
        tabsize: 2,
        height: 100
      });


	  window.pageData = window.pageData || {};

	  window.pageData.CoupanHtml = ` <hr/> <div class="CoupansBox">
					 <div class="uk-grid uk-grid-divider CoupansContainer uk-grid-medium CurrencyCounvertorWrapper" data-uk-grid-margin>
				
							<div class="uk-width-large-9-10">
								<div class="uk-grid uk-grid-divider uk-grid-medium uk-margin" data-uk-grid-margin>
														
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
											<label>Provider name</label>
												<input type="text" class="md-input" name="providername[]"   value="">
										
											</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Website</label>
											<input type="text" class="md-input" name="providerwebsite[]" value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Email</label>
											<input type="email" class="md-input " name="provideremail[]"   value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Phone no</label>
											<input type="text" class="md-input onlynumber" name="providerphoneno[]"  value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider UUID</label>
											<input type="text" class="md-input " name="provideruuid[]"   value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider  Description </label>
										<textarea class="md-input" name="providerdescription[]" ></textarea>
									
										</div>
									</div>
									<div class="uk-width-large-1-3">
										<div class="uk-form-row">
										<label>Provider Type</label>
											<select name="providertype[]" class="md-input"  >
										
											<option name="spot">Spot</option>
											<option name="stem">Stem</option>
											<option name="gymnastics">Gymnastics</option>
											<option name="dance">Dance</option>
											<option name="art">Art</option>
											<option name="theater">Theater</option>
											<option name="other">Other</option>
										

											</select>
									
										</div>
									</div>
									<div class="uk-width-large-1-2">
										<div class="uk-form-row">
										<label>Provider Type option</label>
											<input type="text" class="md-input " name="providertypeoption[]"  value="">
									
										</div>
									</div>
									
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>Street Address</label>
											<input type="text" class="md-input " name="streetaddress[]"  value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>Apt/Room/Description </label>
											<input type="text" class="md-input " name="apt[]"  value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>City</label>
											<input type="text" class="md-input " name="city[]"  value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>State</label>
											<input type="text" class="md-input " name="state[]"  value="">
									
										</div>
									</div>
									<div class="uk-width-large-1-5">
										<div class="uk-form-row">
										<label>Zip</label>
											<input type="text" class="md-input onlynumber" name="zip[]"  value="">
									
										</div>
									</div>
									</div>
								</div>
								<div class="uk-width-large-1-10">
										<div  class="md-btn md-btn-primary removeCoupan" data-uk-tooltip title="Remove Details " style="margin-left: -25px"><i class="material-icons"> close </i></div>
								</div>

									
									

								</div>
							</div>
`;

$('#addCoupansButton').on('click', (e)=>{
            e.preventDefault();
            $e = $(e.currentTarget);
            
            if($e.parents('.CoupansWrapper').find('.CoupansContainer').length < 5){
                $newFields = $(window.pageData.CoupanHtml).appendTo('.CoupansBox');
				$newFields.find('input').val();
			}

       });

	   $(document).on('click', '.removeCoupan', (e)=>{

	
            e.preventDefault();
            $e = $(e.currentTarget);

            if($e.parents('.CoupansBox').find('.CoupansContainer').length > 1){
                $e.parents('.CoupansContainer').remove();

			}
	   });
</script>

@endsection

