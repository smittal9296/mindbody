<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Log;
use App\Mbclass;
class ProductController extends Controller
{
    
public $token="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";

    public function shop(){

        $shopname=Auth::user()->name;
        $token=Auth::user()->password;;
        // $url="https://$shopname/admin/api/2021-01/shop.json";

        $url="https://$shopname/admin/api/2021-01/products.json";
        $shop=$this->apigetcall($token,$url);
          $products=json_decode($shop,1);
         
          return view('index',compact('products'));

    }

 public function product(){  
  // $token="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";
  //     $url="https://testmindbody.myshopify.com/admin/api/2021-01/products.json";
      // $data=[
      //   "product"=>[
      //     "title"=> "Mindbody Product",
      //     "body_html"=> "<strong>MindBody Product Description</strong>",
      //     "vendor"=> "Mindbody",
      //     "product_type"=> "Classes"
      //    ]
      //   ];
       


      $shopname=Auth::user()->name;
      $token=Auth::user()->password;;
      $url="https://$shopname/admin/api/2021-01/products.json";
      $products=$this->apigetcall($token,$url);

      // $products=$this->apipostcall($token,$url,$data);
       $product=json_decode($products,1);
      return view('product',compact('product'));
      
           
    //  return view('index', compact('products'));   


}


public function addproduct(Request $req){

  // dd($req->all());
  $data=$req->all();
  $shopname=Auth::user()->name;
  $token=Auth::user()->password;;
  $url="https://$shopname/admin/api/2021-01/products.json";
   $products=$this->apipostcall($token,$url,$data);
    return $products;
  }


public function apigetcall($token,$url){
         
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 0);
    // curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $headers = ["X-Shopify-Access-Token:$token"];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $server_output = curl_exec ($ch);
    return $server_output;
    
    curl_close ($ch);
}


public function apipostcall($token,$url,$data){

 
 
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));  //Post Fields
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $headers = [
                "X-Shopify-Access-Token:$token",
                "Content-Type: application/json"
               ];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $server_output = curl_exec ($ch);
    return $server_output;
    
    curl_close ($ch);
}


    public function order(){
      $shopname=Auth::user()->name;
            $token=Auth::user()->password;;
            $url="https://$shopname/admin/api/2021-01/orders.json?status=any";

          
            $shop=$this->apigetcall($token,$url);
              $products=json_decode($shop,1);
                return $products;
    }

    public function uninstall(){
      Log::info('uniall');
      Log::info('I am laravel testing');
    }
    public function class(){  
          $shopname=Auth::user()->name;
          $token=Auth::user()->password;
          $url="https://$shopname/admin/api/2021-01/products.json";
          $products=$this->apigetcall($token,$url);
          $product=json_decode($products,1);
          return view('class',compact('product'));


    }
}
