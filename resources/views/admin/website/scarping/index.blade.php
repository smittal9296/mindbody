
 @extends('admin.layouts.base')
 @section('breadcrumb')
<div id="top_bar" >
    
    <ul id="breadcrumbs" >
        <li><a href="{{ url('admin') }}" style=" color: rgba(0, 0, 0, 0.5); font-weight:600;">Dashboard</a></li>
        <li><span style="color:#2dc15f;">Searching</span></li>
    </ul>
    
</div>
@endsection




@section('content')

<form id="form_validation" action="{{URL('admin/scrapingdata')}}" method="POST" class="uk-form-stacked" enctype="multipart/form-data">
	@csrf

	<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
		<div class="uk-width-medium-1-1">
			<div class="md-card" >
				
				<div class="md-card-content large-padding">

                    <div class="uk-width-large-1-2">
                             <div class="uk-form-row">
                                <label>Search Keyword</label>
                                   <input type="text" class="md-input" name="search"   value="" required id="search">
                                                
                                 </div>
                                 <div class="uk-form-row">
							          <button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" style="background:#2dc15f;">  Search  </button>
					    	    </div>
                            </div>
                          
                            <div class="uk-width-large-1-2">
                               
                            </div>
               </div>
            </div>  
        </div>
</div>
</form>   

<br>

@if(isset($data))

<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
		<div class="uk-width-medium-1-1">
			<div class="md-card" >
				
				<div class="md-card-content large-padding">
         
             <table class="uk-table uk-table-align-vertical uk-table-nowrap " id="dt_default">
                 <thead>
                     <tr>
                     
                     <th> <input type="checkbox" id="selectall"></th>
                        
                     <th class=" uk-text-center">Icon</th>     
                         <th  class="uk-text-center"> Name</th>
                        
                         <th class=" uk-text-center" >Website</th> 
                         <th class=" uk-text-center">Phoneno</th>  
                         <th class=" uk-text-center">Address</th> 
                            
                     </tr>
                 </thead>
                 
                 <tbody>
                     @foreach($data->results as $key => $d)
                     
                     @php
                      $ref_p=[];
                      $t_p='';
                      
                        if(isset($d->photos))
                        {
                           
                        $photo=json_encode($d->photos);
                        $p=json_decode($photo,1);
                        
                        foreach($p as $t)
                            {
                                $ref_p[]=$t['photo_reference']; 
                            }
                             
                        }
                        else {
                            $p=[];
                        }
                       
                       $t_p=$ref_p;
                    
                    @endphp
                
                 
                   {{-- <a href="https://maps.googleapis.com/maps/api/place/photo?maxwidth=2448&photoreference=Aap_uEB-lizEFCedfDpLM30LfAZ6pZO3bc6kLb7o1JZpZJTtlgj5sJWp8mhjyiV6tZoVfAHh0drER5TS1dM_U89kqQ8L3fFePfPVRyleOY-UBjQTftLWJzwSdgAw_iwAHeAErHF9RaTfOQ6dJuZeSDuKLC3GR6gbJ8ryGV077ehQv4cF3xPq&sensor=false&key=AIzaSyBOMHLoNP_WyrFtDo5ybtx4QsxSX-sDWz4">img</a> --}}
                     <tr>
                      
                     <td><input type="checkbox" data-website="{{$d->website ?? ''}}"  data-phoneno="{{$d->phoneno ?? ''}}"  data-name="{{$d->name}}" data-address="{{$d->adr_address ?? ''}}" data-img="{{ json_encode($t_p) ?? ''}}" class="checkboxattendence"></td>
                     <td class="uk-text-center" >
                                @isset($p)
                                @if($p!=[])
                                @foreach($p as $t)
                                <a href="https://maps.googleapis.com/maps/api/place/photo?maxwidth=2448&photoreference={{$t['photo_reference']}}&sensor=false&key=AIzaSyBOMHLoNP_WyrFtDo5ybtx4QsxSX-sDWz4" target="_blank"><img src="https://maps.googleapis.com/maps/api/place/photo?maxwidth=100&photoreference={{$t['photo_reference']}}&sensor=false&key=AIzaSyBOMHLoNP_WyrFtDo5ybtx4QsxSX-sDWz4"style="width: 100%;height:60px;"></a>
                                @endforeach
                                {{-- @else 
                                    <input type="file" name="image"> --}}
                                @endif
                                @endisset
                                
                            </td>
                                    
                     <td class="uk-text-centesr">{{$d->name ?? ''}}</td>
                              
                               <td class="uk-text-centesr" style="white-space: pre-line !important;">{{$d->website ?? ''}}</td>
                               <td class="uk-text-centesr">{{$d->phoneno ?? ''}}</td>
                             
                            <td class="uk-text-center">{{$d->formatted_address ?? ''}}</td>
                            

                             
                            
                     </tr>
                     @endforeach
                   
                 </tbody>
             </table>
        
                        
                          <div class="uk-grid">
                            <div class="uk-width-large-1-3">
                            <div class="uk-form-row">
										<label>Client </label>
										  <input type="text" class="md-input " name="shop"   value="reggieapp-com.myshopify.com" required id="shopdata" readonly>
									
										</div>
                           
                            </div>
                            <div class="uk-width-large-1-3">
                            <input type="checkbox" data-switchery data-switchery-size="large" checked name="isshopify" id="switch_demo_large"  />
                            <label for="switch_demo_large" class="inline-label">Upload Shopify</label>
                           
                            </div>
                            <div class="uk-width-large-1-3">
                             
                             <div class="uk-form-row">
                                  <button  class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" style="background:#2dc15f;" id="upload">  Save  </button>
                            </div>
                        </div>
</div>
                        
        
         </div>
         </div>
         </div>
         </div>  
         <form action="{{URL('admin/shopifyscraping')}}" method="POST" style="display: none;">
            @csrf
            <input type="hidden" name="isshopify" id="isshopify"> 
           <input type="hidden" name="name" id="formid">
           <input type="hidden" name="website" id="formid1">
           <input type="hidden" name="shop" id="shopvalue">
           <input type="hidden" name="img" id="img">
             <input type="hidden" name="address" id="address">
             <input type="hidden" name="class" id="class" value="{{$class ?? ''}}">
             
               <input type="hidden" name="phoneno" id="phoneno">
           <input type="submit" name="submit" id="formsubmit">
       
    </form>
@endif


@endsection

@section('js')

<script type="text/javascript">
    $('.isshopify').on('change', function() {
        console.log($('.checkbox_value').val());
        $('.show_image').empty();   
        if($('.checkbox_value').val() == 1){
            $('.checkbox_value').val('0');
            $('.show_image').append(`<input type="text" class="md-input shopdata" name="shop"   value="" id="shopdata" style="margin-top:-25px;">`);
        }else{
            $('.checkbox_value').val('1');
            $('.show_image').append(`<input type="text" class="md-input shopdata" name="shop"   value="" id="shopdata" required style="margin-top:-25px;">`);
        }
    });

   $(document).on('click','#selectall',function(e){
                      
                      if(this.checked) {
                       $('.checkboxattendence').each(function(){
                        $(".checkboxattendence").prop('checked', true);
                    })
                      }else {
                       $('.checkboxattendence').each(function(){
                        $(".checkboxattendence").prop('checked', false);
                    })
                        
                      }                
                  });
        
                $('.checkboxattendence').click(function(){
                      if($(".checkboxattendence").length == $(".checkboxattendence:checked").length) { 
                           //if the length is same then untick 
                          $("#selectall").prop("checked", true);
                      }else {
                          //vise versa
                          $("#selectall").prop("checked", false);            
                      }
                  }); 


                  $('#upload').click(function(e){

                     
                      var isshopify=0;
                      if($('#switch_demo_large').is(':checked')){
                         isshopify=1;
                      }
                         console.log(isshopify); 
                   var i = 0;
                   var s=0;
                   var p=0;
                   var w=0;
                   var n=0;
                   
                   
                   $shop=$('#shopdata').val();
                           
                   if($(".checkboxattendence:checked").length){
                      
                            if($shop=='' && isshopify == 1){
                                    alert('Enter Client information');
                                }
                          else{
                        var arr = [];
                        var arr1 = [];
                        var arr2 = [];
                        var arr3 = [];
                        var arr4 = [];
                        
                       $('.checkboxattendence:checked').each(function () {
                            arr[i++] = $(this).data('name');
                            arr1[s++] = $(this).data('website');
                            arr2[p++] = $(this).data('img');
                             arr3[w++] = $(this).data('address');
                              arr4[n++] = $(this).data('phoneno');
                            
                          }); 
                         
                        //   alert(arr2);
                          $('#isshopify').val(isshopify);
                       $('#formid').val(arr);
                       $('#formid1').val(arr1);
                       $('#address').val(arr3);
                       $('#phoneno').val(arr4);
                       $('#shopvalue').val($shop);
                       $('#img').val(arr2);
                       $('#formsubmit').trigger('click');  

                     
  
                         }
                   }else{
                    alert('Select Atleast 1 record');
                  }
                   
  
          }); 
  
</script>
@endsection