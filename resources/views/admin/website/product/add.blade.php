
 @extends('admin.layouts.base')
 @section('breadcrumb')
<div id="top_bar" >
    
    <ul id="breadcrumbs" >
        <li><a href="{{ url('admin') }}" style=" color: rgba(0, 0, 0, 0.5); font-weight:600;">Dashboard</a></li>
        <li><span style="color:#2dc15f;">{{ isset($data) ? 'Update' : 'Add' }} Product </span></li>
    </ul>
    
</div>
@endsection



@section('css')
<link rel="stylesheet" href="{{ asset('backend/assets/skins/dropify/css/dropify.css') }}">

<link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
@endsection

@section('content')

<form id="form_validation" action="{{URL('admin/product')}}" method="POST" class="uk-form-stacked" enctype="multipart/form-data">
	@csrf

 <input type="hidden" name="id" value="{{$data->id ?? ''}}">
	<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
		<div class="uk-width-medium-1-1">
			<div class="md-card" >
				
				<div class="md-card-content large-padding">
				<div id="CoupansWrapper" class="CoupansWrapper uk-margin-bottom-large">
                    <input type="hidden" name="sectorJsonData" id="coupanJsonData">
				 <div class="CoupansBox">
					 <div class="uk-grid uk-grid-divider CoupansContainer uk-grid-medium CurrencyCounvertorWrapper" data-uk-grid-margin>
				
							<div class="uk-width-large-10-10">
								<div class="uk-grid uk-grid-divider uk-grid-medium uk-margin" data-uk-grid-margin>
														
									<div class="uk-width-large-1-2">
										<div class="uk-form-row">
											<label>Product Title</label>
												<input type="text" class="md-input" name="productname"   value="" required>
										
											</div>
									</div>
									<div class="uk-width-large-1-2">
										<div class="uk-form-row">
										<label>Product type</label>
											<input type="text" class="md-input" name="producttype" value="" >
									
										</div>
									</div>
									<div class="uk-width-large-1-2">
										<div class="uk-form-row">
										<label>Provider</label>
											<input type="text" class="md-input " name="productvendor"   value="" >
									
										</div>
									</div>
									
									<div class="uk-width-large-1-2">
										<div class="uk-form-row">
										<label>Product  Description </label>
										<textarea class="md-input" name="productdescription"  ></textarea>
									
										</div>
									</div>
                                    <div class="uk-width-medium-1-2">
                                       <div class="uk-form-row">
										<label>Client </label>
										  <input type="text" class="md-input " name="shop"   value="" required>
									
										</div>
                                       </div>
									   <div class="uk-width-medium-1-2">
                            <input type="checkbox" data-switchery data-switchery-size="large" checked name="isshopify" id="switch_demo_large"  />
                            <label for="switch_demo_large" class="inline-label">Upload Shopify</label>
                           
                        </div>
									   <div class="uk-width-large-1-2">
											<div class="md-card">
												<div class="md-card-content">
												   <h3 class="heading_a uk-margin-small-bottom">
													Upload Thumbnail
											     	</h3>
													<input type="file" id="input-file-events" class="dropify-event" name="thumbnail" data-max-file-size="400K"  accept="image/*"  data-allowed-file-extensions='["jpg", "png","gif"]' @if(isset($data->thumbnail)) data-default-file="{{ URL('upload/'.$data->thumbnail)}}"   @endif >

													<input type="hidden" name="oldthumbnail" value="{{@$data->thumbnail ?? ''}}">
												</div>
										    </div>
										</div>

										<div class="uk-width-large-1-2">
											<!-- <div class="md-card">
												<div class="md-card-content">
												<h3 class="heading_a uk-margin-small-bottom">
													Upload  Image
												</h3>
													<input type="file" id="input-file-events1" class="dropify-event" name="image" data-max-file-size="400K"  accept="image/*"  data-allowed-file-extensions='["jpg", "png","gif"]' @if(isset($data->image)) data-default-file="{{ URL('upload/'.$data->image)}}"   @endif >

													<input type="hidden" name="oldimage" value="{{@$data->image ?? ''}}">
												</div>
										    </div> -->
										</div>
                          
                      
                                  

                            <div class="uk-form-row uk-margin uk-padding">
							          <button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light" style="background:#2dc15f;">  Save  </button>
					    	</div>
                    	</div>
                    	</div>
                    	</div>
                    	</div>
                        </div>
                    
                        </div>
                        </div>
                        </div>
                        </div>
                    
</form>
@endsection
      

@section('js')

<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
<script src="{{ asset('backend/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/pages/forms_file_input.min.js') }}"></script>
<script type="text/javascript">
    // Used events
                var drEvent = $('#input-file-events').dropify();

                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                // drEvent.on('dropify.afterClear', function(event, element){
                //     alert('File deleted');
                // });

                drEvent.on('dropify.errors', function(event, element){
                    console.log('Has Errors');
                });

				 var drEvent = $('#input-file-events1').dropify();

                drEvent.on('dropify.beforeClear', function(event, element){
                    return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
                });

                // drEvent.on('dropify.afterClear', function(event, element){
                //     alert('File deleted');
                // });

                drEvent.on('dropify.errors', function(event, element){
                    console.log('Has Errors');
                });

</script>


@endsection