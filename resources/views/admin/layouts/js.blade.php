
@php
    $bower_url = url('backend/bower_components');
    $kendo_url = '../backend/bower_components/kendo-ui/js';
    $dist_min = isset($_GET["generate"]) ? '.min' : '';
    $srcAssets = url('backend');
@endphp

<!-- common functions -->
<script src="{{ $srcAssets.'/assets/js/common'.$dist_min.'.js' }}"></script>
<!-- uikit functions -->
<script src="{{ $srcAssets.'/assets/js/uikit_custom'.$dist_min.'.js' }}"></script>
<!-- altair common functions/helpers -->
<script src="{{ $srcAssets.'/assets/js/altair_admin_common'.$dist_min.'.js' }}"></script>

{{-- form js --}}

<script src="{{ $bower_url.'/select2/dist/js/select2.min.js' }}"></script>

{{-- data table --}}
<script src="{{ $bower_url.'/datatables/media/js/jquery.dataTables'.$dist_min.'.js' }}"></script>
<script src="{{ $srcAssets.'/assets/js/custom/datatables/datatables.uikit'.$dist_min.'.js' }}"></script>
<script src="{{ $srcAssets.'/assets/js/pages/plugins_datatables'.$dist_min.'.js' }}"></script>

{{-- toast --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha256-56zsTlMwzGRtLC4t51alLh5cKYvi0hnbhEXQTVU/zZQ=" crossorigin="anonymous"></script>

{{-- table plugins --}}
<script src="{{ asset('backend/bower_components/tablesorter/dist/js/jquery.tablesorter.min.js') }}"></script>
<script src="{{ asset('backend/bower_components/tablesorter/dist/js/jquery.tablesorter.widgets.min.js') }}"></script>
<script src="{{ asset('backend/bower_components/tablesorter/dist/js/widgets/widget-alignChar.min.js') }}"></script>
<script src="{{ asset('backend/bower_components/cropper/dist/cropper.min.js') }}"></script>

{{-- validation --}}

@if (session('success'))
<script type="text/javascript">
    $.toast({
        heading: 'Success',
        text: "{{ session('success') }}",
        icon: 'success',
        loader: true,
        loaderBg: '#2dc15f',
        allowToastClose: true,
        position : 'bottom-left'
    });
</script>
@php
    session()->forget('success');
@endphp
@endif

@if (session('error'))
<script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "{{ session('error') }}",
        icon: 'success',
        loader: true,
        loaderBg: '#ea2739',
        allowToastClose: true,
        position : 'bottom-left'
    });    
</script>
@php
    session()->forget('error');
@endphp
@endif
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-201000146-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

  gtag('config', 'UA-201000146-1');
</script>
        <script src="{{URL('backend/summernote/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{URL('backend/custom/sweetalert2.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
     <script>

            jQuery(document).ready(function(){

                $('.summernote').summernote({
                    height: 100,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
                
                $('.inline-editor').summernote({
                    airMode: true            
                });

            });
    // jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.onlynumber').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode
//  (charCode != 45 || $(element).val().indexOf('-') != -1) && // “-” CHECK MINUS, AND ONLY ONE. 
        if(
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    } 
// End ONlY number Class


  function mydelete(id,table)
        {
            swal({
                
            title:"Are you sure?",
            text:"You won't be able to revert this!",
            type:"warning",
            showCancelButton:true,
            confirmButtonText:"Yes, delete it!",
            confirmButtonClass:"btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",               
            cancelButtonText:"No, thanks",
             closeOnConfirm: false,   
            closeOnCancel: false },function(isConfirm){
              if(isConfirm){
                 var Apiurl =  window.pageData.baseUrl+"/api/CommanDelete";  
                    $.ajax({                                  
                        type: "POST",                                                                          
                        url: Apiurl,                       
                        data: {id:id,table:table }, 
                        success: function (data) 
                        {
                          console.log(data);
                            if(data.status ==  200 )
                            {
                               
                              swal("Deleted!","Your file has been deleted.","success");
                              window.location.reload();
                            }
                            else
                            {
                                swal("Try Again!","Record could not be deleted.","error"); 
                            }                                
                        }   
                            
                                
                    });
                  }else{
                     swal("Cancelled", "Your imaginary file is safe :)", "error"); 
                  }
            })
           

        }
          function mystatus(id,table,action)
           {
            swal({
                
            title:"Are you sure?",
            type:"success",
            showCancelButton:true,
            confirmButtonText:"Yes, Change it!",
            confirmButtonClass:"btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",               
            cancelButtonText:"No, thanks",
             closeOnConfirm: false,   
            closeOnCancel: false },function(isConfirm){
              if(isConfirm){
                 var Apiurl =  window.pageData.baseUrl+"/api/CommanStatus";  
                    $.ajax({                                  
                        type: "POST",                                                                          
                        url: Apiurl,                       
                        data: {id:id,table:table,status:action }, 
                        success: function (data) 
                        {
                          console.log(data);
                            if(data.status ==  200 )
                            {
                               
                              swal("Changed!","Status has been Changed.","success");
                              window.location.reload();
                            }
                            else
                            {
                                swal("Try Again!","Status could not be Changed.","error"); 
                            }                                
                        }   
                            
                                
                    });
                  }else{
                     swal("Cancelled", "Status is not Change", "error"); 
                  }
            })
           

         }


           
        </script>
  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>

<script>
  var OneSignal = window.OneSignal || [];

       OneSignal.push(function() {
        OneSignal.isPushNotificationsEnabled(function(isEnabled) {
          if (isEnabled)
            OneSignal.getUserId(function(playerId) {
                @guest
                @else
                var userId = {{ Auth::user()->id}};
                var playerId = playerId;
                var token = "{{ csrf_token() }}";
                console.log(playerId);
                $.ajax({
                    url:"{{URL('api/setPlayerId/admin')}}",
                    type: 'post',
                   
                    data : {
                        user_id: userId,
                        player_id: playerId,
                        _token: token
                    }, 
                    success: function(data) {
                        console.dir(data); 
                    },
                    error:function(error){
                        console.dir(error);
                    }
                });
                @endguest
                console.log('player_id of the subscribed user is : ' + playerId);
            });
        else
            console.log("Push notifications are not enabled yet.");
    });

    OneSignal.on('subscriptionChange', function (isSubscribed) {
        console.log("The user's subscription state is now:",isSubscribed);
        if(isSubscribed){
            OneSignal.getUserId(function(playerId) {
                @guest
                toastr["warning"]('Please login once for complete subscription', "warning");
                @else
                var userId = {{ Auth::user()->id}};
                var playerId = playerId;
                var token = "{{ csrf_token() }}";
                $.ajax({
                    url:"{{URL('api/setPlayerId/admin')}}",
                    type: 'post',
                    dataType: 'json',
                    data : {
                        user_id: userId,
                        player_id: playerId,
                        _token: token
                    }, 
                    success: function(data) {
                        console.dir(data);  
                    },
                    error:function(error){
                        console.log(error);
                    }
                });
                @endguest
                console.log('player_id of the subscribed user is : ' + playerId);
            }); 
        }
    });


    OneSignal.init({
      appId: "2d63ceda-8ca9-45c5-b687-14677fa63073",
    });
  });
</script>