<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\{User,Admin,Mbclass,Product};
class DashboardController extends Controller
{

    
    public function index(){
        $users[] = Auth::user();
        $users[] = Auth::guard()->user();
        $users[] = Auth::guard('admin')->user();
        $class=Mbclass::count();
        $product=Product::count();
        $title="Dashboard";
        return view('admin.website.index',compact('title','class','product'));
    }

} 