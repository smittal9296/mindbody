<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Hash;
use App\Mail\Forgotmail;
use App\Admin;
use App\User;
use Log;

class HomeController extends Controller
{

public function data(){
  $data=Admin::get();
  return response()->json(['status'=>true,'message'=>'success','data'=>$data]);
}
public function data1(){
  $data=User::get();
  return response()->json(['status'=>true,'message'=>'success1','data'=>$data]);
}

public function resetadminpassword(Request $req){
  $data=Admin::whereEmail($req->email)->first();
  if($data){
    $str=rand(); 
    $token = sha1($str);
    $data->token=$token;
     $data->save();
      Mail::to(trim($data->email))->send(new Forgotmail($data->name,$token));
      return back()->with('success','Check your inbox for a password reset email'); 
  }else{
    return back()->with('error','Email Not Found');
  }
}


  public function resetpasswordemail(Request $req){
        
      $data=Admin::where('token',$req->token)->first();
   
    if($data){
       $data->password=\Bcrypt($req->confirmpassword);
       $data->save();
        return redirect('/admin/login')->with('success','Successfully Change your Password'); 
       }else{
        return back()->with('error','Sorry Token Not Match'); 
  
       } 
    }



}