<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Scraping extends Authenticatable
{
    protected $table="scraping";
    public $timestamps=false;

}
