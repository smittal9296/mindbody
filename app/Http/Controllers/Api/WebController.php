<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class WebController extends Controller
{
   
    public function CommanDelete(Request $req){
    try{
     
      $class='App\\'.$req->table;
      $data=$class::find($req->id);
      if($data){
           
           $data->delete();


	      // $data->isDeleted=1;
	      // $data->isActive=0;
          // $data->save();
       return response()->json(['status'=>200,'Message'=>'Successfully Deleted','data'=>$data]);
     }else{
      return response()->json(['status'=>404,'Message'=>'Data not Found']);
     }
   }catch(\Exception $e){
    return response()->json(['status'=>500,'Message'=>'Server not response' ,'data'=>$e->getMessage()]);
   }
  }
   // Status Change isActive
   public function CommanStatus(Request $req){
       try{
             if(!empty($req->id)){
               $class='App\\'.$req->table;
                   $check=$class::find($req->id);
                    if($check){
                        $check->isActive=$req->status;
                        $check->save();
                        return response()->json(['status'=>200,'Message'=>'Successfully Status change','data'=>$check]);

                    }else{
                      return response()->json(['status'=>204,'Message'=>'Data not Found']);
                    }  
             }else{
              return response()->json(['status'=>'201','Message'=>'Param Missing']);
             }  
       }catch(\Exception $e){
          return response()->json(['status'=>500,'Message'=>'Server not Response','data'=>$e->getMessage()]);
       }   
   }



}
