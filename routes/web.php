<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
// */

Route::get('/', function () {
  return view('welcome');
  })->middleware(['verify.shopify'])->name('home');

  Route::get('/shop','ProductController@shop')->middleware(['verify.shopify']);

  Route::get('/products','ProductController@product')->middleware(['verify.shopify']);
   Route::get('/order','ProductController@order')->middleware(['verify.shopify']);
   Route::get('/class','ProductController@class')->middleware(['verify.shopify']);
  
  Route::get('/uninstall','ProductController@uninstall');

//  Route::get('/', function () {
//    return view('welcome');
//    })->name('home');
  
Route::get('test','HomeController@data1')->name('home1');
  // Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');

Route::view('internet','errors.internet');
Route::POST('resetadminpassword','HomeController@resetadminpassword');
Route::POST('resetpasswordemail','HomeController@resetpasswordemail')->name('resetpasswordemail');

Route::get('forgotadmintoken/{token}',function($token){
  return view('admin.auth.resetpassword',compact('token'));
});
    
Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');

Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  // Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
  // Route::post('/register', 'AdminAuth\RegisterController@register');

  // Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  // Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
  // Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  // Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
