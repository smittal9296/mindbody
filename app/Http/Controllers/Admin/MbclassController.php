<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\{User,Mbclass,Product,Scraping};
use Log;
use Storage;
class MbclassController extends Controller
{

    public function index(){
        $title="Mbclass";
        $data=Mbclass::orderBy('id','desc')->get();
        return view('admin.website.mbclass.index',compact('data','title'));
  
      }
  
      public function create(){
         $title="Mbclass";
        
          $data='';
          return view('admin.website.mbclass.create',compact('title','data'));
        
      }
  
      public function edit($id){

        
        $title="Mbclass";
        $data=Mbclass::find(base64_decode($id));

        if($data){
                  return view('admin.website.mbclass.create',compact('data','title'));
        }else{
               return back()->with('success','No record found');
        }
        
      }
  
      public function save(Request $req){
         
        $data=User::where('name',$req->shop)->first();
        if(isset($req->isshopify)){
          if(!$data){
            return redirect('/admin')->with('error','No Shop record found');
          }
        }
        $check=Mbclass::where('id','!=',$req->id)->where('providername',$req->providername)->first();
        if($check){
          return redirect('/admin')->with('error','providername already exists');
        }
            $title="Mbclass"; 
          if($req->id != ''){
            $insert=Mbclass::find($req->id);
            } else{        
            $insert=new Mbclass();
            }
                 

          $insert->providername=$req->providername;
          $insert->providerwebsite=$req->providerwebsite;
          $insert->provideremail=$req->provideremail;
          $insert->providerphoneno=$req->providerphoneno;
          $insert->provideruuid=$req->provideruuid;
          $insert->providerdescription=$req->providerdescription;
          $insert->providertype=$req->providertype;
          $insert->providertypeoption=$req->providertypeoption;
          $insert->streetaddress=$req->streetaddress;
          $insert->apt=$req->apt;
          $insert->city=$req->city;
          $insert->state=$req->state;
          $insert->zip=$req->zip;


          $insert->classname=$req->classname;
          $insert->classdate=$req->classdate;
          $insert->classage=$req->classage;
          $insert->classweek=$req->classweek;
          $insert->classtime=$req->classtime;
          $insert->classseason=$req->classseason;
          $insert->classstreetaddress=$req->classstreetaddress;
          $insert->classapt=$req->classapt;
          $insert->classcity=$req->classcity;
          $insert->classstate=$req->classstate;
          $insert->classzip=$req->classzip;
          $insert->classfrequency=$req->classfrequency;
          $insert->classprice=$req->classprice;
          $insert->spacesavailable=$req->spacesavailable;
          $insert->discountcode=$req->discountcode;
          $insert->waiverlink=$req->waiverlink;
          $insert->waitlist=$req->waitlist;
          $insert->rules=$req->rules;
          $insert->classdescription=$req->classdescription;
          $insert->cancellationpolicy=$req->cancellationpolicy;
          $insert->makeuppolicy=$req->makeuppolicy;
          $insert->soldout=$req->soldout;
          $insert->classtype=$req->classtype;
          $insert->spotsavailable=$req->spotsavailable;
          $insert->classsneighborhoods=$req->classsneighborhoods;
          

          if($req->hasFile('providerlogo')){		
            $path = public_path(). '/upload/';
            $image = $req->file('providerlogo');
            $imagename =date('dmyhis').'.'.$image->getClientOriginalExtension();
            $image->move($path, $imagename);
            $insert->providerlogo =  "https://dev1.algoseedlabs.com/upload/$imagename";
          
                   }else{
                     $insert->providerlogo=$req->oldproviderlogo;
                   }
                   if($req->hasFile('providerimage')){		
                    $path = public_path(). '/upload/';
                    $image = $req->file('providerimage');
                    $imagename =date('dmyhis').'.'.$image->getClientOriginalExtension();
                    $image->move($path, $imagename);
                    $insert->providerimage = $imagename;
                           }else{
                             $insert->providerimage=$req->oldproviderimage;
                           }
          

        
          $insert->save();
          $add=$req->streetaddress." ".$req->classcity;
          if(isset($req->isshopify)){
             
            $description=$req->streetaddress.'-'.$req->city.'-'.$req->state.'-'.$req->zip.'<br>'.$req->providerphoneno.'<br><a href="'.$req->providerwebsite.'">'.$req->providerwebsite.'</a>'.'<br><br>'.$req->classdescription;
            // dd($description);    
            $data1=[     
                      "product"=>[
                        "title"=>$req->providername,
                        "body_html"=>$description              
                        
                       ]
                    ];      
             
             
              $token=$data->password;
              $pid='';
              if($req->productid){
              $url="https://$req->shop/admin/api/2021-01/products/$req->productid.json";
              $products=$this->apiputcall($token,$url,$data1);
              Log::info('productdata',array($products));
              $pid=$req->productid;
              }else{
                $url="https://$req->shop/admin/api/2021-01/products.json";  
                $products=$this->apipostcall($token,$url,$data1);      
                Log::info('productdata',array($products));      
                $product=json_decode($products,1);
                $pid=$product['product']['id'];
              }
             
            
              $iu=URL('/upload/').'/'.$insert->providerlogo;
              if($req->hasFile('providerlogo')){
                $data1=[
                  "image"=>["id" => $req->imageid,"src"=> $insert->providerlogo]
                  ];

                 
                
                
              if($req->imageid){
               
                $url="https://$req->shop/admin/api/2021-01/products/$pid/images/$req->imageid.json";
                // $url="https://$req->shop/admin/api/2021-01/products/$pid/images.json";
               
                $image=$this->apiputcall($token,$url,$data1);
                // dd($image);
              } else{
                $image=$this->apipostcall($token,$url,$data1);
                $url="https://$req->shop/admin/api/2021-01/products/$pid/images.json";
                $image=$this->apipostcall($token,$url,$data1);
                // dd($image);
              }
               
              // dd($insert->providerlogo);
               
               
            
               
                }
        }
      
        //  $this->link($req->mobileno,$req->name,$addressfinal,$link);
  
          return redirect('admin/mbclass')->with('success','Succefully add class');
      }


      // public function product(Request $req){
         
    
      //   $pro=new Product;
      //   $pro->name=$req->productname;
      //   $pro->type=$req->producttype;
      //   $pro->vendor=$req->productvendor;
      //   $pro->description=$req->productdescription;
      //   $pro->shop=$req->shop;
      //   $data=User::where('name',$req->shop)->first();
      //           if(!$data){
      //             return back()->with('error','No Shop record found');
      //           }


      //           if($req->hasFile('thumbnail')){		
      //             $path = public_path(). '/upload/';
      //             $image = $req->file('thumbnail');
      //             $imagename =date('dmyhis').'.'.$image->getClientOriginalExtension();
      //             $image->move($path, $imagename);
      //             $pro->thumbnail = $imagename;
      //           }
      
      //   if(isset($req->isshopify)){
      //       $iu=URL('/upload/').'/'.$pro->thumbnail;
          
      //          $data1=[
      //               "product"=>[
      //               "title"=>$req->productname,
      //               "body_html"=> $req->productdescription,
      //               "vendor"=>$req->productvendor,
      //               "product_type"=> $req->producttype
      //               ]
      //             ];
      //     $pro->isshopify=1;
      //     $token=$data->password;;
      //     $url="https://$req->shop/admin/api/2021-01/products.json";
      //     $products=$this->apipostcall($token,$url,$data1);
    
           
      //     if($products){
      //         $product=json_decode($products,1);
      //          $pro->productdetails=json_encode($product['product']);
      //          $pro->save();
      //          if($pro->thumbnail){
      //          $pid=$product['product']['id'];
             
      //          $url="https://$req->shop/admin/api/2021-01/products/$pid/images.json";

      //          $data1=[
      //            "image"=>["src"=>$iu]
      //            ];
      //          $image=$this->apipostcall($token,$url,$data1);
      //          }
      //       return back()->with('success','Product Successfull add');
      //     }else{
      //       return back()->with('success','Sorry Some error Found'); 
      //     }
         
      //   }
        
      // }




      public function apibitly($token,$url,$data1){

 
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data1));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $headers = [
                    "Authorization:$token",
                    "Content-Type: application/json"
                   ];
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec ($ch);
        return json_decode($server_output,1);
        
        curl_close ($ch);
    }

      public function apipostcall($token,$url,$data1){

 
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
  
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data1));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $headers = [
                    "X-Shopify-Access-Token:$token",
                    "Content-Type: application/json"
                   ];
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec ($ch);
        return $server_output;
        
        curl_close ($ch);
    }
    public function apiputcall($token,$url,$data1){

 
 
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data1));  //Post Fields
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      
      $headers = [
                  "X-Shopify-Access-Token:$token",
                  "Content-Type: application/json"
                 ];
  
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      $server_output = curl_exec ($ch);
      return $server_output;
      
      curl_close ($ch);
  }
   

    public function scraping(){
      $title="Scraping";
     
      return view('admin.website.scarping.index',compact('title'));
    }

    public function scrapingdata(Request $req){
      // $key=config('services.google.key');
    $key="AIzaSyBOMHLoNP_WyrFtDo5ybtx4QsxSX-sDWz4";
      $queryurl="https://maps.googleapis.com/maps/api/place/textsearch/json?query=$req->search&key=$key";
      $urls=preg_replace("/ /", "%20", $queryurl);
      
      // $nearbyurl="'https://maps.googleapis.com/maps/api/place/nearbysearch/json?type=$req->search&keyword=$req->search&key=AIzaSyA1b7xvtOPiI0yYoLxzJDj2DBiqS4udYvA";
      
      $data1=file_get_contents($urls);
      $data=json_decode($data1);
     
     
         

        if($data->status=="OK"){
            // for($i=0;$i<count($data->results);$i++ ){
            //   dd($data[$i]);
            // }
              // dd($data);
            foreach($data->results as $d){
                $url="https://maps.googleapis.com/maps/api/place/details/json?placeid=$d->place_id&sensor=false&key=$key";
                $getdata=file_get_contents($url);
                $value=json_decode($getdata,1);
                 if($value['status']=="OK"){
                                 
                  if(isset($value['result']['adr_address'])){
                     
                             $adr=(explode(',',$value['result']['adr_address']));
                             $size=count($adr);
                              if($size >0){
                                $streetadd=isset($adr[0]) ? strip_tags($adr[0]) : '';
                                 $city=isset($adr[1]) ? strip_tags($adr[1]) : '';
                                 $code= isset($adr[2]) ? strip_tags($adr[2]) : '';
                                 $d->adr_address=$streetadd.'-'.$city.'-'.$code;
                            
                              }
                              
                              
                  }
                  $d->website= isset($value['result']['website']) ? $value['result']['website'] :'';
                  $d->phoneno= isset($value['result']['formatted_phone_number']) ? $value['result']['formatted_phone_number'] :'';            
                 }
               }
                            
          $class=$req->search;
      
          return view('admin.website.scarping.index',compact('data','class'));
        }else{
         return back()->with('success',$data->error_message);
        }
    }



    public function shopifyscraping(Request $req){
      // dd($req->all());
           $data=User::where('name',$req->shop)->first();
           if($req->isshopify){
                if(!$data){
                  return redirect('/admin/scraping')->with('error','No Shop record found');
                }
              }   
          
                 $name=explode(',',$req->name);
                 $address=explode(',',$req->address);
                 $phoneno=explode(',',$req->phoneno);
                 $website=explode(',',$req->website);
                 $img=explode(',',$req->img);
        
              // $sp=new Scraping;
              // $sp->name=json_encode($name);
              // $sp->address=json_encode($address);
              // $sp->isshopify=$req->isshopify;
              // $sp->shop=$req->shop;
              // $sp->save();
             
               
              for($i=0;$i<count($name);$i++){
              
               $address[$i]=isset($address[$i]) ? $address[$i]:'';
               $name[$i]=isset($name[$i]) ? $name[$i]:'';
               $phoneno[$i]=isset($phoneno[$i]) ? $phoneno[$i]:'';
               $website[$i]=isset($website[$i]) ? $website[$i]:'';
               $img[$i]=isset($img[$i]) ? $img[$i]:'';
               $description='';
               $imgurl="https://maps.googleapis.com/maps/api/place/photo?maxwidth=2448&photoreference=$img[$i]&sensor=false&key=AIzaSyBOMHLoNP_WyrFtDo5ybtx4QsxSX-sDWz4";
                 
                $url1="https://api-ssl.bitly.com/v4/bitlinks";
                $token1="Bearer 4190ada4c456f03c3fa9ef75f965d9c2966d3f70";
                $datawebsite=array('long_url'=>$website[$i]);
                $link=$website[$i];
                $bitly=$this->apibitly($token1,$url1,$datawebsite);
 
               if(isset($bitly['link'])){
                 $link=$bitly['link'];
               }
             

                // $contents = file_get_contents($imgurl);
                // $name1 = substr($imgurl, strrpos($imgurl, '/') + 1);
                // $na=rand(1,666).'.png';
                // Storage::disk('public')->put($na, $contents);
               $description=$address[$i].'<br>'.$phoneno[$i].'<br><a href="'.$link.'">'.$link.'</a>';
               $check=Mbclass::where('providername',$name[$i])->first();
               if($check){}
               else{
                $insert=new Mbclass();
                 $insert->providername=$name[$i];
                 $insert->providerwebsite=$link;
                 $insert->providerphoneno=$phoneno[$i];
                //  $insert->providerdescription=$description;
                 $newadd=explode('-',ltrim($address[$i]));
                 $zip=isset($newadd[2]) ? ltrim($newadd[2]):''; 
                
                 $insert->streetaddress=isset($newadd[0]) ? $newadd[0]:'' ;
                 $insert->city= isset($newadd[1]) ? $newadd[1]: '' ;
                       if($zip != ''){
                           $zipdivide=explode(' ',$zip);
                           if(isset($zipdivide)){
                              $insert->state=isset($zipdivide[0]) ? $zipdivide[0] : '';
                              $insert->zip=isset($zipdivide[1]) ? $zipdivide[1] : '';
                          }
                        }
                 $insert->classname=$req->class;
                 $insert->shop=$req->shop;         
                 $insert->providerlogo=$imgurl;
              
               
                if($req->isshopify){
                  $data1=[
                      "product"=>[
                        "title"=>$name[$i],
                        "body_html"=>$description,
                       ]
                    ];      
             
             
               $token=$data->password;
                 $url="https://$req->shop/admin/api/2021-01/products.json";
                 $products=$this->apipostcall($token,$url,$data1);
// dd($products);
                 if($products){
                  $product=json_decode($products,1);
                 
                    $pid=$product['product']['id'];
                    Log::info('productdata',array($pid));
                    $insert->productid=$pid;
                   
                    $url="https://$req->shop/admin/api/2021-01/products/$pid/images.json";
                        $data1=[
                          "image"=>["src"=>$imgurl]
                          ];
                       $image=$this->apipostcall($token,$url,$data1);
                      
                        if(isset($image)){
                          $ig=json_decode($image,1);
                          if(isset($ig['image']['id'])){
                            $imageid=$ig['image']['id'];
                            $insert->imageid=$imageid;
                          }
                         }
                         $insert->save();
                       
                 
                     }
                  }
                }
        }
          return redirect('/admin/scraping')->with('success','Product upload successfully');
    }


    public function scrapinglist(){
      $title="Scrapinglist";
       $data=Scraping::all();
  
       return view('admin.website.scarping.list',compact('data','title'));
    }

    public function multipledelete(Request $r){
      // dd($r->id);
      $ids = explode(',', $r->id);

      foreach($ids as $id){
        $data=Mbclass::find($id);
        if($data){
             $data->delete();
       }else{
        return redirect('/admin/mbclass')->with('success','Class delete successfully');
       }
      }
      return redirect('/admin/mbclass')->with('success','Class delete successfully');
    }



    public function cronbitly(){
      $data=Mbclass::all();
      foreach($data as $d){ 
        if($d->providerwebsite){

          $biturl=str_replace('https://','',$d->providerwebsite);
          

           $url="https://api-ssl.bitly.com/v4/bitlinks/$biturl/clicks?unit=month";
                
           $token="Bearer 4190ada4c456f03c3fa9ef75f965d9c2966d3f70";
           $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL,$url);
          curl_setopt($ch, CURLOPT_POST, 0);
          // curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         
          $headers = [
            "Authorization:$token",
            "Content-Type: application/json"
           ];
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          $server_output = curl_exec ($ch);
            $js= json_decode($server_output,1);
           $count=$js['link_clicks']['0']['clicks'];
          curl_close ($ch);
            $mb=Mbclass::find($d->id);
            $mb->totalclick=$count;
            $mb->save();
        } 
      }
      return redirect('/admin');
    }


    public function resetlink($id){
      $data=Mbclass::find($id);
      if($data){
        $data->totalclick=0;
        $data->save();
        return back()->with('error','Reset Links Successfully');
      }else{
        return back()->with('error','Sorry Id not found');
      }

    }

    public function resetlinks(Request $r){
      // dd($r->id);
      $ids = explode(',', $r->id);

      foreach($ids as $id){
        $data=Mbclass::find($id);
        if($data){
             $data->totalclick=0;
             $data->save();
         }else{
        return redirect('/admin/mbclass')->with('success','Record  not Found');
       }
      }
      return redirect('/admin/mbclass')->with('success','Reset Click successfully');
    }


    public function csvdata(){
            try{   
              
      //  $fileName=public_path().'/report'.rand(1,100).'.csv';


       //  dd($fileurl);
            // $files = glob(public_path('/order/*'));
            //     // get all file names
            //     foreach($files as $file){ // iterate files
            //      if(is_file($file)) {
            //         unlink($file); // delete file
            //         }
            //    }       
             $fileName = 'report.csv';

            $headers = array(
               "Content-type"        => "text/csv",
               "Content-Disposition" => "attachment; filename=$fileName",
               "Pragma"              => "no-cache",
               "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
               "Expires"             => "0"
                );
           $columns = array('providername', 'Providerwebsite','Class name','Provider Phoneno','TotalClick');
    
                      $data=Mbclass::all();
                     $callback = function() use($data, $columns) {
                        // $file = fopen($fileName, 'ab+');
                        $file = fopen('php://output', 'w');
                          fputcsv($file, $columns);
                          foreach($data as $d){
                               fputcsv($file, array($d->providername,$d->providerwebsite,$d->classname,$d->providerphoneno,$d->totalclick));
                             
                           }
                           

                           fclose($file);


                           
                       
                  };
                  // return response()->download($fileName);
                   return \Response::stream($callback, 200, $headers);
      // return   response()->stream($callback, 200, $headers);
  //  return "Order csv successfully Save in ".$fileName;
}catch(\Exception $e){
  echo $e->getMessage();
} 

   


    }




} 