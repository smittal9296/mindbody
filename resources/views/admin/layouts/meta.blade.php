        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <meta name="description" content=" Application"> -->
        <meta name="author" content="Skmittal">
      <!--    <link rel="icon" type="image/png" sizes="32x32" href="{{URL('favicon/favicon-32x32.png')}}">
         <link rel="icon" type="image/png" sizes="96x96" href="{{URL('favicon/favicon-96x96.png')}}">
         <link rel="icon" type="image/png" sizes="16x16" href="{{URL('favicon/favicon-16x16.png')}}"> -->
         <!-- <link rel="shortcut icon" href="{{URL('favicon/favicon-96x96.png')}}"> -->
         <title>{{ config('app.name', 'Laravel') }}</title>
         <!-- <meta http-equiv="refresh" content="300"> -->
         <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--      <meta property="og:type" content="CardApp" />
         <meta property="og:title" content="Application" />
         <meta property="og:url" content="" />
         <meta property="og:site_name" content="Card" />
     -->     
     {{-- <meta property="og:image" content="https://via.placeholder.com/150/0000FF/808080?Text=Card "> --}}
        