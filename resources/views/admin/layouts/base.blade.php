{{-- Admin main layout --}}

@php define('safe_access',true); @endphp
<!DOCTYPE html>

<html lang="">
<head>
   
    @include('admin.layouts.meta')
    <title> {{ config('app.name', 'Laravel') }} </title>
    <link rel="shortcut icon" href="{{ asset('favicon/favicon.png') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('assets/favicon/favicon.png') }}" type="image/x-icon">
    <script>
      window.pageData = window.pageData || {};
      window.pageDate = window.pageData || {};
      window.pageData.baseUrl="{{ url('/') }}";
    
      window.pageData.token = "{{ csrf_token() }}";
    </script>
    @if(Auth::guard('admin')->user())
    <script>
      window.pageData.user = "{!! base64_encode(json_encode(Auth::guard('admin')->user())) !!}";
    </script>
    @endif


    @include('admin.layouts.css')
    @yield('css')
</head>
<body class="sidebar_main_open sidebar_main_swipe">
        
    <div class="wrapper">
        @include('admin.layouts.header')
        @include('admin.layouts.leftsidebar')

        <div id="page_content">
            @yield('breadcrumb')
            @yield('pageTitle')
            <div id="page_content_inner">
            @yield('content')
            </div>
        </div>

        @yield('modals')
    </div>

    @include('admin.layouts.js')
    {{-- @include('admin.layouts.admin_js') --}}
    @yield('js')
</body>
</html>