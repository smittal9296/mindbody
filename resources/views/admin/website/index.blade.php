@extends('admin.layouts.base')
 @section('breadcrumb')
<div id="top_bar" >
    
    <ul id="breadcrumbs" >
        <li><a href="{{ url('admin') }}" style=" color: rgba(0, 0, 0, 0.5); font-weight:600;">Dashboard</a></li>
        <li><span style="color:#2dc15f;">Welcome to {{ config('app.name', 'Laravel') }} </span></li>
    </ul>
    
</div>
@endsection
@section('content')

<style type="text/css">
    
    /* rect {
    fill:#157270;
} */
</style>

         

<section class="card-grid" style="margin-top: 10px;">
    <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-4 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
        <div>
            <div class="md-card">
                <div class="md-card-content" style="padding: 10% 5%;">
                 <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">{{rand(1,8)}},3,{{rand(1,8)}},6,{{rand(4,8)}},{{rand(1,8)}},6</span></div>
                    <span class="uk-text-muted uk-text-large">Total Classes</span>
                    <h2 class="uk-margin-remove"><span class="countUpMe" id="orders">{{$class ?? '0'}}</span></h2>
                </div>
            </div>
        </div>
        <div>
            
        </div>
        
       
  
    </div>
   
  
     
       
       
   
</section>
@endsection

@section('js')
<script src="{{ asset('backend/bower_components/chartist/dist/chartist.js') }}"></script>
<script src="{{ asset('backend/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.js') }}"></script>
<script src="{{ asset('backend/bower_components/countUp.js/dist/countUp.js') }}"></script>
<script src="{{ asset('backend/bower_components/peity/jquery.peity.js') }}"></script>
<script src="{{ asset('backend/assets/js/pages/dashboard.js') }}"></script>    
<script>


</script>

@endsection