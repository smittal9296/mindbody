<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Mbclass;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
        $data=Mbclass::all();
        foreach($data as $d){ 
          if($d->providerwebsite){
            $biturl=str_replace('https://','',$d->providerwebsite); 
             $url="https://api-ssl.bitly.com/v4/bitlinks/$biturl/clicks?unit=month";
             $token="Bearer 4190ada4c456f03c3fa9ef75f965d9c2966d3f70";
              $ch = curl_init();
             curl_setopt($ch, CURLOPT_URL,$url);
             curl_setopt($ch, CURLOPT_POST, 0);
            // curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $headers = [
              "Authorization:$token",
              "Content-Type: application/json"
             ];
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
              $server_output = curl_exec ($ch);
              $js= json_decode($server_output,1);
              $count=$js['link_clicks']['0']['clicks'];
              curl_close ($ch);
              $mb=Mbclass::find($d->id);
              $mb->totalclick=$count;
              $mb->save();
          } 
        }
       })->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
