

 @extends('admin.layouts.base')
 @section('breadcrumb')
<div id="top_bar" >
    
    <ul id="breadcrumbs" >
        <li><a href="{{ url('admin') }}" style=" color: rgba(0, 0, 0, 0.5); font-weight:600;">Dashboard</a></li>
        <li><span style="color:#2dc15f;">Scraping Data</span></li>
    </ul>
    
</div>
@endsection




@section('content')
<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
		<div class="uk-width-medium-1-1">
			<div class="md-card" >
				
				<div class="md-card-content large-padding">
         
             <table class="uk-table uk-table-align-vertical uk-table-nowrap " id="dt_default">
                 <thead>
                     <tr>
                     
                     <th> Sr</th>
                         <th  class="uk-text-center"> Shop</th>
                         <th  class="uk-text-center"> Name</th>
                         <th class=" uk-text-center">Address</th> 
                         <th>Shopify Upload</th>    
                         
                            
                     </tr>
                 </thead>
                 
                 <tbody>
                     
                     @foreach($data as $key => $d)
                     <tr>
                      
                     <td>{{$key+1}}</td>
                     <td>{{$d->shop ?? ''}}</td>
                              <td class="uk-text-center">{{str_replace(array('[',']','"'),"",$d->name) ?? ''}}</td>
                                                
                           
                               <td class="uk-text-center">
                                {{str_replace(array('[',']','"'),"",$d->address) ?? ''}}
                             </td>
                             <td class="uk-text-center">
                            {{$d->isshopify ==1 ? 'Uploaded':'Not Uploaded'}}
                             </td>
                             
                            

                             
                            
                     </tr>
                     @endforeach
                   
                 </tbody>
             </table>
        
                        
                        
                        
        
                  </div>
              </div>
             </div>
         </div>  

@endsection
