<?php defined('safe_access') or die('Restricted access!'); ?>

<div id="page_content">
	<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <div class="heading_actions">
            <a href="#" data-uk-tooltip="{pos:'bottom'}" title="Users"><i class="md-icon material-icons">group</i></a>
            <a href="#" data-uk-tooltip="{pos:'bottom'}" title="Settings"><i class="md-icon material-icons">settings</i></a>
            <div data-uk-dropdown>
                <i class="md-icon material-icons">&#xE5D4;</i>
                <div class="uk-dropdown uk-dropdown-small">
                    <ul class="uk-nav">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Other Action</a></li>
                        <li><a href="#">Other Action</a></li>
                    </ul>
                </div>
            </div>
        </div>
		<h1>Community Forum</h1>
		<span class="uk-text-muted uk-text-upper uk-text-small">123 users online</span>
	</div>
	<div id="page_content_inner">
        <ul class="uk-breadcrumb">
            <li><a href="">Forum</a></li>
            <li><a href="">General Discussions</a></li>
            <li class="uk-active"><span>Thread</span></li>
        </ul>
        <div class="uk-grid">
            <div class="uk-width-medium-2-3">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-flex uk-flex-middle">
                            <div class="uk-flex-item-1">
                                <h2 class="heading_b">General Discussions</h2>
                                <span class="uk-text-muted">134 users online</span>
                            </div>
                            <div>
                                <i class="md-icon material-icons">&#xE5D5;</i>
                                <div class="uk-position-relative uk-display-inline-block" data-uk-dropdown="{pos:'bottom-right'}">
                                    <i class="md-icon material-icons">&#xE5D4;</i>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav">
                                            <li><a href="#">Action 1</a></li>
                                            <li><a href="#">Action 2</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <ul class="md-list md-list-addon uk-margin-top">
<?php
for($i=0;$i<6;$i++) {
?>
                            <li>
                                <div class="md-list-addon-element">
                                    <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_0<?php echo $i + 2;?>_tn.png" alt=""/>
                                </div>
                                <div class="md-list-content">
                                    <div class="uk-flex uk-flex-middle">
                                        <div class="uk-width-8-10">
                                            <h4 class="uk-margin-remove uk-text-truncate">
                                                <a href="page_forum_single.html" class="uk-link-muted"><?php echo $faker->sentence(5);?></a>
                                            </h4>
                                            <div>
                                                <?php if($i==0){?><span class="uk-badge uk-badge-danger uk-float-none uk-badge-inline uk-margin-small-right">Sticky</span><?php };?>
                                                <span class="uk-text-muted">Most recent by <a href="" class="uk-link-muted"><?php echo $faker->name;?></a></span>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-10">
                                            <i class="material-icons md-color-green-500 md-24">remove_red_eye</i> <?php echo rand(1,100);?>
                                        </div>
                                        <div class="uk-width-1-10">
                                            <i class="material-icons md-color-amber-500 md-24">comment</i> <?php echo rand(1,200);?>
                                        </div>
                                    </div>
                                </div>
                            </li>
<?php }; ?>
                        </ul>
                    </div>
                </div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-flex uk-flex-middle">
                            <div class="uk-flex-item-1">
                                <h2 class="heading_b">Product Reviews</h2>
                                <span class="uk-text-muted">74 users online</span>
                            </div>
                            <div>
                                <i class="md-icon material-icons">&#xE5D5;</i>
                                <div class="uk-position-relative uk-display-inline-block" data-uk-dropdown="{pos:'bottom-right'}">
                                    <i class="md-icon material-icons">&#xE5D4;</i>
                                    <div class="uk-dropdown">
                                        <ul class="uk-nav">
                                            <li><a href="#">Action 1</a></li>
                                            <li><a href="#">Action 2</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <ul class="md-list md-list-addon uk-margin-top">
				            <?php
				            for($i=1;$i<5;$i++) {
					            ?>
                                <li>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_0<?php echo $i + 2;?>_tn.png" alt=""/>
                                    </div>
                                    <div class="md-list-content">
                                        <div class="uk-flex uk-flex-middle">
                                            <div class="uk-width-8-10">
                                                <h4 class="uk-margin-remove uk-text-truncate">
                                                    <a href="page_forum_single.html" class="uk-link-muted"><?php echo $faker->sentence(5);?></a>
                                                </h4>
                                                <div>
										            <?php if($i==0){?><span class="uk-badge uk-badge-danger uk-float-none uk-badge-inline uk-margin-small-right">Sticky</span><?php };?>
                                                    <span class="uk-text-muted">Most recent by <a href="" class="uk-link-muted"><?php echo $faker->name;?></a></span>
                                                </div>
                                            </div>
                                            <div class="uk-width-1-10">
                                                <i class="material-icons md-color-green-500 md-24">remove_red_eye</i> <?php echo rand(1,100);?>
                                            </div>
                                            <div class="uk-width-1-10">
                                                <i class="material-icons md-color-amber-500 md-24">comment</i> <?php echo rand(1,200);?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
				            <?php }; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-3">
                <div class="md-card">
                    <div class="md-card-content">
                        <h2 class="heading_b">Categories</h2>
                        <ul class="uk-nav uk-nav-side uk-nav-parent-icon uk-margin-top">
                            <li><a href="#">General Discussions</a></li>
                            <li class="uk-parent uk-open">
                                <a href="#">Product Reviews</a>
                                <ul class="uk-nav-sub">
                                    <li>
                                        <a href="#">Samsung</a>
                                        <ul>
                                            <li><a href="#">Mobile Phones</a></li>
                                            <li><a href="#">TV</a></li>
                                            <li><a href="#">Sound System</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Apple</a>
                                        <ul>
                                            <li class="uk-active"><a href="#">iPhone</a></li>
                                            <li><a href="#">iPad</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="md-card">
                    <div class="md-card-content">
                        <h2 class="heading_b">Tags</h2>
                        <div class="uk-margin uk-badge-group">
                            <span class="uk-badge uk-badge-default uk-badge-large"><a href="#">SEO (12)</a></span>
                            <span class="uk-badge uk-badge-large uk-badge-danger"><a href="#">Samsung (43)</a></span>
                            <span class="uk-badge uk-badge-default uk-badge-large"><a href="#">Mobile (8)</a></span>
                            <span class="uk-badge uk-badge-default uk-badge-large"><a href="#">Import (3)</a></span>
                            <span class="uk-badge uk-badge-large uk-badge-success"><a href="#">Apple (23)</a></span>
                            <span class="uk-badge uk-badge-default uk-badge-large"><a href="#">Language (1)</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
