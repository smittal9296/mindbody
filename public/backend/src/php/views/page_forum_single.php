<?php defined('safe_access') or die('Restricted access!'); ?>

<div id="page_content">
	<div id="page_content_inner">

		<ul class="uk-breadcrumb">
			<li><a href="">Forum</a></li>
			<li><a href="">General Discussions</a></li>
			<li class="uk-active"><span>Single Thread</span></li>
		</ul>

		<div class="uk-grid">
			<div class="uk-width-medium-4-5">
				<div class="md-card">
					<div class="md-card-content large-padding">
						<h2 class="heading_b uk-margin-small-bottom"><?php echo $faker->sentence(8);?></h2>
						<div class="uk-text-muted"><span class="uk-margin-right">July 2018</span> In: <a href="#"class="uk-link-muted">General</a></div>
						<div class="uk-margin-large-top">
							<div class="forum-single-item">
								<div class="uk-flex">
									<div class="uk-margin-right">
										<img class="md-user-image uk-margin-small-top" src="assets/img/avatars/avatar_04_tn.png" alt=""/>
									</div>
									<div class="uk-flex-item-1">
                                        <div class="uk-margin-bottom">
                                            <h4 class="uk-margin-remove"><?php echo $faker->name;?></h4>
                                            <span class="uk-text-muted"><?php
                                                $moment = new Moment\Moment();
                                                echo $moment->subtractDays(10)->format('j M');?></span>
                                        </div>
                                        <p><?php echo $faker->sentence(60); ?></p>
                                        <hr class="md-hr">
									</div>
								</div>
							</div>
                            <p class="uk-text-muted uk-text-upper uk-margin-top-remove uk-margin-large-bottom">Comments</p>
<?php for($i=0;$i<6;$i++) { ?>
                            <div class="forum-single-item">
                                <div class="uk-flex">
                                    <div class="uk-margin-right">
                                        <img class="md-user-image uk-margin-small-top" src="assets/img/avatars/avatar_0<?php echo 4 + $i; ?>_tn.png" alt=""/>
                                    </div>
                                    <div class="uk-flex-item-1">
                                        <div class="uk-margin-bottom">
                                            <div class="uk-flex">
                                                <div class="uk-flex-item-1">
                                                    <h4 class="uk-margin-remove"><?php echo $faker->name;?></h4>
                                                    <span class="uk-text-muted"><?php $moment = new Moment\Moment();echo $moment->subtractDays((11 + $i))->subtractHours(rand(1,8))->format('j M H:i');?></span>
                                                </div>
                                                <div>
                                                    <a href="#"><i class="md-icon material-icons">reply</i></a>
                                                    <a href="#"><i class="md-icon material-icons">link</i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <p><?php echo $faker->sentence(60); ?></p>
                                        <hr>
                                    </div>
                                </div>
                            </div>
<?php }; ?>
                            <p class="uk-text-muted uk-text-upper">Add comment</p>
                            <div>
                                <label for="forum-replay">Your comment...</label>
                                <textarea name="forum-reply" id="forum-replay" cols="20" rows="4" class="md-input"></textarea>
                                <button class="md-btn md-btn-primary uk-margin-top">Send</button>
                            </div>
						</div>
					</div>
				</div>
			</div>
			<div class="uk-width-medium-1-5">
                <div class="md-card">
                    <div class="md-card-head">
                        <div class="md-card-head-menu" data-uk-dropdown="{pos:'bottom-right'}">
                            <i class="md-icon material-icons">&#xE5D4;</i>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav">
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Remove</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="uk-text-center">
                            <img class="md-card-head-avatar" src="assets/img/avatars/avatar_03.png" alt=""/>
                        </div>
                        <h3 class="md-card-head-text uk-text-center">
							<?php echo $faker->name; ?>
                            <span class="uk-text-truncate"><?php echo $faker->company; ?></span>
                        </h3>
                    </div>
                    <div class="md-card-content">
                        <ul class="md-list">
                            <li>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Info</span>
                                    <span class="uk-text-small uk-text-muted"><?php echo $faker->sentence(10);?></span>
                                </div>
                            </li>
                            <li>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Email</span>
                                    <span class="uk-text-small uk-text-muted uk-text-truncate"><?php echo $faker->email; ?></span>
                                </div>
                            </li>
                            <li>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Phone</span>
                                    <span class="uk-text-small uk-text-muted"><?php echo $faker->phoneNumber; ?></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>