@php
$srcAssets = url('backend');    
@endphp

    <!-- main header -->
    <header id="header_main" style="background: #157270">
        <div class="header_main_content">
            <nav class="uk-navbar">
                
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
                
                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>
                

                   
                
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        
                       <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                       <!-- <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">2</span> </a> -->
                            <div class="uk-dropdown uk-dropdown-xlarge">
                                <div class="md-card-content">
                                    <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                        <li class="uk-width-1-1 uk-active"><a href="#" class="js-uk-prevent uk-text-small" >Notifications </a></li>
                                        {{-- <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Stock  <sup style="color:red;">()</sup> </a></li> --}}
                                    </ul>
                                    <ul id="header_alerts" class="uk-switcher uk-margin">
                                        <li> 

                                         
                                            <ul class="md-list md-list-addon">
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <span class="md-user-letters md-bg-cyan"></span> 
                                                    </div>
                                                    <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#" style="color:#157270;font-size:14px;">hello</a></span>
                                                        <span class="uk-text-small uk-text-muted">Welcome </span>
                                                    </div>
                                                </li>
                                                
                                            </ul>

                                            
                                            <!-- <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                                <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                            </div> -->
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="{{ asset('image/avatar.png') }}" alt="Admin"/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <!-- <li><a href="">My profile</a></li> -->
                                    {{-- <li><a href="javascript:;">Settings</a></li> --}}
                                    <li>
                                      <a class="" href="{{ URL('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">  <i class="fa fa-lock"></i>  &nbsp;{{ __('Sign out') }} </a>
                                        <form id="logout-form" action="{{ URL('admin/logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
                <script type="text/autocomplete">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">
                        {{-- {{~items}} --}}
                        <li data-value="34">
                            <a href="sjkdhfjh" class="needsclick">
                                34<br>
                                <span class="uk-text-muted uk-text-small">dkjghk</span>
                            </a>
                        </li>
                        {{-- {{/items}} --}}
                        <li data-value="autocomplete-value">
                            <a class="needsclick">
                                Autocomplete Text<br>
                                <span class="uk-text-muted uk-text-small">Helper text</span>
                            </a>
                        </li>
                    </ul>
                </script>
            </form>
        </div>
    </header><!-- main header end -->